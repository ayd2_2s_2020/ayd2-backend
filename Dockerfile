FROM  maven:3.6.2-jdk-8-slim as builder
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8-alpine 
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/target/koala-backend-0.0.1-SNAPSHOT.jar /usr/src/app/app.jar  
COPY --from=builder /usr/src/app/pom.xml /usr/src/app/pom.xml
COPY --from=builder /usr/src/app/src /usr/src/app/src
EXPOSE 8080  
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/usr/src/app/app.jar"] 
#ENTRYPOINT [ "tail" , "-f", "/dev/null" ]